<?php

namespace App\Acme\Commanding;

interface CommandHandler
{
    public function handle($command);
}
