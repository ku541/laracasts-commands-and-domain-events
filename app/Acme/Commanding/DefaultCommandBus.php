<?php

namespace App\Acme\Commanding;

use App\Acme\Commanding\CommandBus;
use App\Acme\Commanding\CommandTranslator;

class DefaultCommandBus implements CommandBus
{
    protected $commandTranslator;

    public function __construct(CommandTranslator $commandTranslator)
    {
        $this->commandTranslator = $commandTranslator;
    }

    public function execute($command)
    {
        $handler = $this->commandTranslator->toCommandHandler($command);

        return app()->make($handler)->handle($command);
    }
}
