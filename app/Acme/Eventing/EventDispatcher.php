<?php

namespace App\Acme\Eventing;

use Illuminate\Events\Dispatcher;

class EventDispatcher
{
    protected $event;

    public function __construct(Dispatcher $event)
    {
        $this->event = $event;
    }

    public function dispatch(array $events)
    {
        foreach ($events as $event) {
            $eventName = $this->getEventName($event);

            $this->event->fire($eventName, $event);

            logger("$eventName was fired.");
        }
    }

    protected function getEventName($event)
    {
        return str_replace('\\', '.', get_class($event));
    }
}
