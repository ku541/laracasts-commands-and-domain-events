<?php

namespace App\Acme\Eventing;

use ReflectionClass;

class EventListener
{
    public function handle($eventName, $event)
    {
        $eventName = $this->getEventName($event);
        
        if ($this->listenerIsRegistered($eventName)) {
            return call_user_func([$this, 'when'.$eventName], array_first($event));
        }
    }

    public function getEventName($event)
    {
        return (new ReflectionClass(array_first($event)))->getShortName();
    }

    public function listenerIsRegistered($eventName)
    {
        $method = "when{$eventName}";

        return method_exists($this, $method);
    }
}
