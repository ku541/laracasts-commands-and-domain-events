<?php

namespace App\Acme\Eventing;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class EventingServiceProvider extends ServiceProvider
{
    public function register()
    {
        $listeners = config('acme.listeners');

        foreach ($listeners as $listener) {
            Event::listen('App.Acme.*', $listener);
        }
    }
}
