<?php

namespace App\Acme\Jobs;

use App\Acme\Eventing\EventGenerator;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use EventGenerator;

    public $table = 'jobs';
    protected $title;
    protected $description;
    protected $fillable = ['title', 'description'];

    public static function post($title, $description)
    {
        $job = static::create(compact('title', 'description'));

        $job->raise(new JobWasPosted($job));

        return $job;
    }

    public function archive()
    {
        $this->delete();

        $this->raise(new JobWasFilled($this));
    }
}
