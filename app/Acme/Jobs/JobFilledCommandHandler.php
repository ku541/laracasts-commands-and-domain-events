<?php

namespace App\Acme\Jobs;

use App\Acme\Eventing\EventDispatcher;
use App\Acme\Commanding\CommandHandler;

class JobFilledCommandHandler implements CommandHandler
{
    protected $job;
    protected $dispatcher;

    public function __construct(Job $job, EventDispatcher $dispatcher)
    {
        $this->job = $job;
        $this->dispatcher = $dispatcher;
    }

    /**
     * Handle the command.
     *
     * @param $command
     * @return mixed
     */
    public function handle($command)
    {
        $job = $this->job->findOrFail($command->jobId);

        $job->archive();

        $this->dispatcher->dispatch($job->releaseEvents());
    }
}
