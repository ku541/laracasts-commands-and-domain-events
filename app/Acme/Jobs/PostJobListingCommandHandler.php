<?php

namespace App\Acme\Jobs;

use App\Acme\Jobs\Job;
use App\Acme\Eventing\EventDispatcher;
use App\Acme\Commanding\CommandHandler;

class PostJobListingCommandHandler implements CommandHandler
{
    protected $job;
    protected $dispatcher;

    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function handle($command)
    {
        dd('Handling post job listing command.');
        $job = Job::post($command->title, $command->description);

        $this->dispatcher->dispatch($job->releaseEvents());
    }
}
