<?php

namespace App\Acme\Jobs;

use App\Acme\Jobs\PostJobListingCommand;
use Illuminate\Support\Facades\Validator;

class PostJobListingValidator
{
    protected static $rules = [
        'title'         => 'required',
        'description'   => 'required'
    ];

    public function validate(PostJobListingCommand $command)
    {
        $validator = Validator::make([
            'title'         => $command->title,
            'description'   => $command->description
        ], static::$rules);

        if ($validator->fails()) {
            dd('Validation Failed');
        }
    }
}
