<?php

namespace App\Acme\Listeners;

use App\Acme\Jobs\JobWasPosted;
use App\Acme\Jobs\JobWasFilled;
use App\Acme\Eventing\EventListener;

class EmailNotifier extends EventListener
{
    public function whenJobWasPosted(JobWasPosted $event)
    {
        dump('Send confirmation email about event: ' . $event->job->title);
    }

    public function whenJobWasFilled(JobWasFilled $event)
    {
        dump('Send congratulations email to the employer about the job: ' . $event->job->title);
    }
}
