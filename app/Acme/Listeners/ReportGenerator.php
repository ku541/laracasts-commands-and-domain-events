<?php

namespace App\Acme\Listeners;

use App\Acme\Jobs\JobWasPosted;
use App\Acme\Eventing\EventListener;

class ReportGenerator extends EventListener
{
    public function whenJobWasPosted(JobWasPosted $event)
    {
        dump('Generate report about event: ' . $event->job->title);
    }
}
