<?php

namespace App\Http\Controllers;

use App\Acme\Commanding\ValidationCommandBus;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $commandBus;

    public function __construct(ValidationCommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }
}
