<?php

namespace App\Http\Controllers;

use App\Acme\Jobs\JobFilledCommand;
use App\Acme\Jobs\PostJobListingCommand;

class JobsController extends Controller
{
    /**
     * Post a new job listing.
     *
     * @return Illuminate\Http\Response
     */
    public function store()
    {
        $input = request()->input();

        $command = new PostJobListingCommand($input['title'], $input['description']);

        $this->commandBus->execute($command);
    }

    /**
     * Set job as filled.
     */
    public function delete($jobId)
    {
        $command = new JobFilledCommand($jobId);

        $this->commandBus->execute($command);
    }
}
