<?php

return [
    'listeners' => [
        'App\Acme\Listeners\EmailNotifier',
        'App\Acme\Listeners\ReportGenerator'
    ]
];
